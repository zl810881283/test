var dgram = require('dgram');
var server = dgram.createSocket('udp4');

server.on("listening", () => {

})

let count = 0
let delay = 0
server.on("message", (msg, rinfo) => {
  count++
  let data = JSON.parse(msg)
  let currDelay = +Date.now() - data.time;
  delay = delay * ((count - 1) / count) + currDelay * (1 / count)
  if (count % 100 == 0) {
    console.log(`time: ${new Date()},currDelay: ${currDelay},avgDelay: ${delay}`);
    count = 0;
    delay = 0;
  }
})
server.bind(12345);