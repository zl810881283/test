var kcp = require('node-kcp');
var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var clients = {};
var interval = 10;
let nextIntervalList = []
var output = function (data, size, context) {
  server.send(data, 0, size, context.port, context.address);
};

server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});
let count = 0;
let delay = 0;

server.on('message', (msg, rinfo) => {
  var k = rinfo.address + '_' + rinfo.port;
  if (undefined === clients[k]) {
    var context = {
      address: rinfo.address,
      port: rinfo.port
    };
    var kcpobj = new kcp.KCP(123, context);
    kcpobj.nodelay(1, interval, 2, 1);
    kcpobj.output(output);
    clients[k] = kcpobj;
  }
  var kcpobj = clients[k];
  kcpobj.input(msg);

  for (var k in clients) {
    var kcpobj = clients[k];
    var recv = kcpobj.recv();

    if (recv) {
      let data = JSON.parse(recv)
      count++
      let currDelay = +new Date() - data.time;
      delay = delay * ((count - 1) / count) + currDelay * (1 / count)
      if (data.idx % 50 == 0) {
        console.log(`time: ${new Date()},${recv.toString()},currDelay: ${currDelay},avgDelay: ${delay}`);
        count = 0;
        delay = 0;
        console.log(`check interval avg: ${nextIntervalList.reduce((i, j) => i + j) / nextIntervalList.length}`);
        nextInterval = []
      }
      kcpobj.send(recv);
      kcpobj.flush();
    }
  }
});

server.on('listening', () => {
  var address = server.address();
  console.log(`server listening ${address.address} : ${address.port}`);

  let nextInterval = interval;

  function update(nextInterval) {

    setTimeout(() => {
      for (var k in clients) {
        var kcpobj = clients[k];
        kcpobj.update(Date.now());
        nextInterval = kcpobj.check(+Date.now());
        nextIntervalList.push(nextInterval)
      }
      update()
    }, nextInterval);
  }

  update(nextInterval)




});

server.bind(41234);
