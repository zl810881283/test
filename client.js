var kcp = require('node-kcp');
var kcpobj = new kcp.KCP(123, { address: '127.0.0.1', port: 41234 });
var dgram = require('dgram');
var client = dgram.createSocket('udp4');
var msg = 'hello world';
var idx = 1;
var interval = 10;

kcpobj.nodelay(1, interval, 2, 1);

kcpobj.output((data, size, context) => {
  client.send(data, 0, size, context.port, context.address);
});

client.on('error', (err) => {
  console.log(`client error:\n${err.stack}`);
  client.close();
});

client.on('message', (msg, rinfo) => {
  kcpobj.input(msg);
  kcpobj.update(Date.now());
});
let nextInterval = interval;

setInterval(() => {
  kcpobj.update(Date.now());
  nextInterval = kcpobj.check(+Date.now());
}, nextInterval);

setInterval(() => {
  kcpobj.send(JSON.stringify({ msg, idx: idx++, time: +new Date() }));
  kcpobj.flush()
}, interval * 2);

kcpobj.send(JSON.stringify({ msg, idx: idx++ }));